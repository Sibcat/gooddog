//
//  BreedDetailsViewController.swift
//  GoodDog
//
//  Created by Lena on 16.11.17.
//  Copyright © 2017 skvortsovaeo. All rights reserved.
//

import UIKit
import Kingfisher

class BreedDetailsViewController: UIViewController {
  
  private let imageView = UIImageView()
  private let imageUpdatingButton = UIButton(type: .system)
  
  private let viewModel: BreedDetailsViewModelProtocol
  
  init(viewModel: BreedDetailsViewModelProtocol) {
    self.viewModel = viewModel
    super.init(nibName: nil, bundle: nil)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = UIColor.white
    
    setup()
    bindToViewModel()
    
    updateImageView()
  }
  
  private func setup() {
    addImageView()
    addImageUpdatingButton()
  }
  
  private func addImageView() {
    view.addSubview(imageView)
    
    imageView.translatesAutoresizingMaskIntoConstraints = false
    imageView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
    imageView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
    imageView.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor, constant: 20).isActive = true
    imageView.heightAnchor.constraint(equalTo: imageView.widthAnchor).isActive = true
    
    imageView.contentMode = .scaleAspectFit
    imageView.image = #imageLiteral(resourceName: "placeholder")
    imageView.kf.indicatorType = .activity
  }
  
  private func addImageUpdatingButton() {
    view.addSubview(imageUpdatingButton)
    
    imageUpdatingButton.translatesAutoresizingMaskIntoConstraints = false
    imageUpdatingButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    imageUpdatingButton.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 20).isActive = true
    imageUpdatingButton.heightAnchor.constraint(equalToConstant: 44)
    
    imageUpdatingButton.setTitle("More of this breed", for: .normal)
    imageUpdatingButton.addTarget(self, action: #selector(updateImageView), for: .touchUpInside)
  }
  
  private func bindToViewModel() {
    viewModel.didFinishRequest = { [weak self] in
      self?.imageView.kf.indicator?.stopAnimatingView()
    }
    
    viewModel.didReceiveError = { [weak self] error in
      self?.showAlert(forError: error)
      self?.imageUpdatingButton.isEnabled = true
    }
    
    viewModel.didUpdateImage = { [weak self] imageURL in
      if let url = URL(string: imageURL.urlString) {
        guard let `self` = self else { return }
        let currentImage = self.imageView.image
        self.imageView.kf.setImage(with: url, placeholder: currentImage) { [weak self] _, _, _, _ in
          self?.imageUpdatingButton.isEnabled = true
        }
      }
    }
  }
  
  @objc private func updateImageView() {
    imageUpdatingButton.isEnabled = false
    imageView.kf.indicator?.startAnimatingView()
    viewModel.updateImage()
  }
}
