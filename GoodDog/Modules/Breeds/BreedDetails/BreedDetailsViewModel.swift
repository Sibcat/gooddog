//
//  BreedDetailsViewModel.swift
//  GoodDog
//
//  Created by Lena on 16.11.17.
//  Copyright © 2017 skvortsovaeo. All rights reserved.
//

import Foundation

protocol BreedDetailsViewModelProtocol: class {
  var didFinishRequest: (() -> Void)? { get set }
  var didUpdateImage: ((_ imageURL: ImageURLHolder) -> Void)? { get set }
  var didReceiveError: ((_ error: Error) -> Void)? { get set }
  
  func updateImage()
}

class BreedDetailsViewModel: BreedDetailsViewModelProtocol {
  
  typealias Dependencies = HasBreedService
  private let dependencies: Dependencies
  
  private let breed: Breed
  
  init(breed: Breed, dependencies: Dependencies) {
    self.breed = breed
    self.dependencies = dependencies
  }
  
  var didFinishRequest: (() -> Void)?
  var didUpdateImage: ((_ imageURL: ImageURLHolder) -> Void)?
  var didReceiveError: ((_ error: Error) -> Void)?
  
  func updateImage() {
    dependencies.breedService.fetchImageURL(for: breed)
      .then { imageURL -> Void in
        self.didUpdateImage?(imageURL)
      }.catch { error in
        self.didReceiveError?(error)
      }.always {
        self.didFinishRequest?()
    }
  }
}
