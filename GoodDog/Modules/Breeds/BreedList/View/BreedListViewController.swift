//
//  BreedListViewController.swift
//  GoodDog
//
//  Created by Lena on 14.11.17.
//  Copyright © 2017 skvortsovaeo. All rights reserved.
//

import UIKit

class BreedListViewController: UIViewController {
  
  private let itemSpacing: CGFloat = 8
  private let refreshControl = UIRefreshControl()
  private let collectionView: UICollectionView
  private let emptyView = EmptyDataSetView()

  fileprivate let viewModel: BreedListViewModelProtocol
  
  init(viewModel: BreedListViewModelProtocol) {
    self.viewModel = viewModel
    
    let layout = UICollectionViewFlowLayout()
    layout.minimumLineSpacing = itemSpacing
    layout.minimumInteritemSpacing = itemSpacing
    collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
    
    super.init(nibName: nil, bundle: nil)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setup()
    bindToViewModel()
    
    viewModel.updateBreeds()
  }
  
  private func setup() {
    addCollectionView()
    addEmptyDataView()
  }
  
  private func addCollectionView() {
    view.addSubview(collectionView)
    
    collectionView.translatesAutoresizingMaskIntoConstraints = false
    collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
    collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
    collectionView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
    collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    
    collectionView.register(BreedCell.self, forCellWithReuseIdentifier: BreedCell.reuseIdentifier)
    collectionView.dataSource = self
    collectionView.delegate = self
    
    collectionView.alwaysBounceVertical = true
    
    collectionView.backgroundColor = UIColor.groupTableViewBackground
    
    addRefreshControl()
  }
  
  private func addEmptyDataView() {
    collectionView.backgroundView = emptyView
    emptyView.isHidden = true
    
    emptyView.title = "No doggos"
    emptyView.buttonConfiguration = EmptyDataSetButtonConfiguration(title: "Show me doggos", action: { [weak self] in
      self?.viewModel.updateBreeds()
    })
  }
  
  private func addRefreshControl() {
    refreshControl.addTarget(self, action: #selector(handlePullToRefresh(_:)), for: .valueChanged)
    collectionView.addSubview(refreshControl)
  }
  
  @objc func handlePullToRefresh(_ sender: UIRefreshControl) {
    viewModel.updateBreeds()
  }
  
  private func bindToViewModel() {
    viewModel.didUpdateBreeds = { [weak self] in
      self?.collectionView.reloadData()
    }
    
    viewModel.didReceiveError = { [weak self] error in
      self?.showAlert(forError: error)
    }
    
    viewModel.didStartRequest = { [unowned self] in
      self.emptyView.isHidden = true
      self.refreshControl.beginRefreshing()
    }
    
    viewModel.didFinishRequest = { [weak self] in
      guard let `self` = self else { return }
      self.refreshControl.endRefreshing()
      self.emptyView.isHidden = !self.viewModel.isEmpty
    }
  }
}

extension BreedListViewController: UICollectionViewDelegate, UICollectionViewDataSource,
  UICollectionViewDelegateFlowLayout {
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return viewModel.breedCount
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    return collectionView.dequeueReusableCell(withReuseIdentifier: BreedCell.reuseIdentifier, for: indexPath)
  }
  
  func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell,
                      forItemAt indexPath: IndexPath) {
    guard let breedCell = cell as? BreedCell, let breed = viewModel.breed(at: indexPath) else {
      return
    }
  
    viewModel.updateImage(at: indexPath) { [weak breedCell] imageURL in
      breedCell?.updateImage(with: imageURL)
    }
    
    breedCell.configure(with: breed)
  }
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    viewModel.selectBreed(at: indexPath)
  }
  
  func collectionView(_ collectionView: UICollectionView,
                      layout collectionViewLayout: UICollectionViewLayout,
                      sizeForItemAt indexPath: IndexPath) -> CGSize {
    let itemWidth = view.bounds.width / 2 - itemSpacing
    return CGSize(width: itemWidth, height: itemWidth)
  }
}
