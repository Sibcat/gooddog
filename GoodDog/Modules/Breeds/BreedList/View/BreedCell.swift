//
//  BreedCell.swift
//  GoodDog
//
//  Created by Lena on 15.11.17.
//  Copyright © 2017 skvortsovaeo. All rights reserved.
//

import UIKit
import Kingfisher

class BreedCell: UICollectionViewCell {
  
  static let placeholder: UIImage? = #imageLiteral(resourceName: "placeholder")
  
  private let nameLabel = UILabel()
  private let dogImageView = UIImageView()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setup()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  private func setup() {
    addImageView()
    addLabel()
  }
  
  private func addImageView() {
    addSubview(dogImageView)
    dogImageView.translatesAutoresizingMaskIntoConstraints = false
    dogImageView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
    dogImageView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
    dogImageView.topAnchor.constraint(equalTo: topAnchor).isActive = true
    dogImageView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    
    dogImageView.contentMode = .scaleAspectFit
    dogImageView.kf.indicatorType = .activity
    dogImageView.image = BreedCell.placeholder
  }
  
  private func addLabel() {
    addSubview(nameLabel)
    nameLabel.translatesAutoresizingMaskIntoConstraints = false
    nameLabel.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
    nameLabel.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
    nameLabel.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    nameLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true
    
    nameLabel.backgroundColor = UIColor.white
    nameLabel.textAlignment = .center
  }
  
  func updateImage(with image: ImageURLHolder?) {
    guard let urlString = image?.urlString else {
      return
    }
    let url = URL(string: urlString)
    dogImageView.kf.setImage(with: url, placeholder: BreedCell.placeholder)
  }
  
  func configure(with breed: Breed) {
    nameLabel.text = breed.name
  }
  
  override func prepareForReuse() {
    super.prepareForReuse()
    dogImageView.kf.cancelDownloadTask()
    dogImageView.image = BreedCell.placeholder
  }
}
