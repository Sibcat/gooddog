//
//  BreedListViewModel.swift
//  GoodDog
//
//  Created by Lena on 15.11.17.
//  Copyright © 2017 skvortsovaeo. All rights reserved.
//

import Foundation
import PromiseKit

protocol BreedListViewModelProtocol: class {
  
  var breedCount: Int { get }
  var isEmpty: Bool { get }
  
  var didUpdateBreeds: (() -> Void)? { get set }
  var didReceiveError: ((_ error: Error) -> Void)? { get set }
  var didStartRequest: (() -> Void)? { get set }
  var didFinishRequest: (() -> Void)? { get set }
  
  func updateBreeds()
  func breed(at indexPath: IndexPath) -> Breed?
  func updateImage(at indexPath: IndexPath, _ completionHandler: @escaping (_ imageURL: ImageURLHolder?) -> Void)
  func selectBreed(at indexPath: IndexPath)
}

protocol BreedListViewModelDelegate: class {
  func breedListViewModel(_ viewModel: BreedListViewModel, didSelectBreed breed: Breed)
}

class BreedListViewModel: BreedListViewModelProtocol {
  
  typealias Dependencies = HasBreedService
  private let dependencies: Dependencies
  
  private var breeds: RealmMappedCollection<RealmBreed, Breed>
  
  private var breedImageURLs: [Breed: ImageURLHolder] = [:]
  
  var didUpdateBreeds: (() -> Void)?
  var didReceiveError: ((_ error: Error) -> Void)?
  var didStartRequest: (() -> Void)?
  var didFinishRequest: (() -> Void)?
  
  weak var delegate: BreedListViewModelDelegate?
  
  var breedCount: Int {
    return breeds.count
  }
  
  var isEmpty: Bool {
    return breeds.isEmpty
  }
  
  init(dependencies: Dependencies) {
    self.dependencies = dependencies
    breeds = dependencies.breedService.fetchBreeds()
  }
  
  func updateBreeds() {
    dependencies.breedService.updateBreeds()
      .then { _ in
        self.didUpdateBreeds?()
      }.catch { error -> Void in
        self.didReceiveError?(error)
      }.always {
        self.didFinishRequest?()
    }
  }
  
  func breed(at indexPath: IndexPath) -> Breed? {
    return breeds.item(at: indexPath.row)
  }
  
  func updateImage(at indexPath: IndexPath, _ completionHandler: @escaping (_ imageURL: ImageURLHolder?) -> Void) {
    guard let breed = breeds.item(at: indexPath.row) else {
      return
    }
    
    if let imageURL = breedImageURLs[breed] {
      completionHandler(imageURL)
      return
    }
    
    dependencies.breedService.fetchImageURL(for: breed)
      .then { imageURLHolder -> Void in
      self.breedImageURLs[breed] = imageURLHolder
      completionHandler(imageURLHolder)
      }.catch { _ in
        completionHandler(nil)
      }
  }
  
  func selectBreed(at indexPath: IndexPath) {
    guard let breed = breeds.item(at: indexPath.row) else {
      return
    }
    delegate?.breedListViewModel(self, didSelectBreed: breed)
  }
}
