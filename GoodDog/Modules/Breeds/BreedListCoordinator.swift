//
//  BreedListCoordinator.swift
//  GoodDog
//
//  Created by Lena on 14.11.17.
//  Copyright © 2017 skvortsovaeo. All rights reserved.
//

import UIKit

class BreedListCoordinator: Coordinating {
  
  private let navigationController: UINavigationController
  private let dependencyContainer: AppDependencyContainer
  
  init(navigationController: UINavigationController, dependencyContainer: AppDependencyContainer) {
    self.navigationController = navigationController
    self.dependencyContainer = dependencyContainer
  }
  
  func start() {
    let viewModel = BreedListViewModel(dependencies: dependencyContainer)
    viewModel.delegate = self
    let viewController = BreedListViewController(viewModel: viewModel)
    viewController.title = "Breeds"
    navigationController.pushViewController(viewController, animated: true)
  }
  
  private func showBreedDetails(_ breed: Breed) {
    let viewModel = BreedDetailsViewModel(breed: breed, dependencies: dependencyContainer)
    let viewController = BreedDetailsViewController(viewModel: viewModel)
    viewController.title = breed.name
    navigationController.pushViewController(viewController, animated: true)
  }
}

extension BreedListCoordinator: BreedListViewModelDelegate {
  func breedListViewModel(_ viewModel: BreedListViewModel, didSelectBreed breed: Breed) {
    showBreedDetails(breed)
  }
}
