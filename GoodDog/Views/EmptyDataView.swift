//
//  EmptyDataSetView.swift
//  GoodDog
//
//  Created by Lena on 17.11.17.
//  Copyright © 2017 skvortsovaeo. All rights reserved.
//

import UIKit

struct EmptyDataSetButtonConfiguration {
  let title: String
  let action: (() -> Void)
}

class EmptyDataSetView: UIView {
  
  var title: String? {
    didSet {
      titleLabel.text = title
      titleLabel.isHidden = title == nil
    }
  }
  
  var buttonConfiguration: EmptyDataSetButtonConfiguration? {
    didSet {
      guard let configuration = buttonConfiguration else {
        actionButton.isHidden = true
        return
      }
      
      actionButton.setTitle(configuration.title, for: .normal)
      actionButton.isHidden = false
    }
  }
  
  private let stackView = UIStackView()
  private let titleLabel = UILabel()
  private let actionButton = UIButton(type: .system)
  
  init(title: String? = nil, buttonConfiguration: EmptyDataSetButtonConfiguration? = nil) {
    self.title = title
    self.buttonConfiguration = buttonConfiguration
    super.init(frame: .zero)
    setup()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  private func setup() {
    addStackView()
    addTitleLabel()
    addButton()
  }
  
  private func addStackView() {
    addSubview(stackView)
    stackView.translatesAutoresizingMaskIntoConstraints = false
    let leadingConstraint = stackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 32)
    leadingConstraint.priority = UILayoutPriority(999)
    leadingConstraint.isActive = true
    
    let trailingConstraint = stackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 32)
    trailingConstraint.priority = UILayoutPriority(999)
    trailingConstraint.isActive = true
    
    stackView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
    stackView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
    
    stackView.axis = .vertical
    stackView.spacing = 20
  }
  
  private func addTitleLabel() {
    stackView.addArrangedSubview(titleLabel)
    titleLabel.numberOfLines = 0
    titleLabel.textAlignment = .center
    titleLabel.textColor = UIColor.darkGray
    titleLabel.font = UIFont.systemFont(ofSize: 22)
  }
  
  private func addButton() {
    stackView.addArrangedSubview(actionButton)
    
    actionButton.translatesAutoresizingMaskIntoConstraints = false
    actionButton.heightAnchor.constraint(equalToConstant: 44)
    
    actionButton.addTarget(self, action: #selector(handleActionButtonTap), for: .touchUpInside)
  }
  
  @objc func handleActionButtonTap(_ sender: Any) {
    buttonConfiguration?.action()
  }
}
