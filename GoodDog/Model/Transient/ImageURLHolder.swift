//
//  ImageURLHolder.swift
//  GoodDog
//
//  Created by Lena on 17.11.17.
//  Copyright © 2017 skvortsovaeo. All rights reserved.
//

import Foundation

struct ImageURLHolder {
  let urlString: String
}
