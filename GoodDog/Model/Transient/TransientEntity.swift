//
//  TransientEntity.swift
//  GoodDog
//
//  Created by Lena on 17.11.17.
//  Copyright © 2017 skvortsovaeo. All rights reserved.
//

import Foundation

protocol TransientEntity {
  associatedtype RealmType: RealmEntity
}
