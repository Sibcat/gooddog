//
//  Breed.swift
//  GoodDog
//
//  Created by Lena on 15.11.17.
//  Copyright © 2017 skvortsovaeo. All rights reserved.
//

import Foundation

func == (lhs: Breed, rhs: Breed) -> Bool {
  return lhs.name == rhs.name
}

struct Breed: Equatable, Hashable, TransientEntity {
  typealias RealmType = RealmBreed
  
  let name: String
  
  var hashValue: Int {
    return name.hashValue
  }
}
