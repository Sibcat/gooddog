//
//  BreedRealm.swift
//  GoodDog
//
//  Created by Lena on 17.11.17.
//  Copyright © 2017 skvortsovaeo. All rights reserved.
//

import Foundation
import RealmSwift

final  class RealmBreed: Object, RealmEntity {
  typealias TransientType = Breed
  
  @objc dynamic var name = ""
  
  override static func primaryKey() -> String? {
    return "name"
  }
  
  class func from(transient: Breed) -> RealmBreed {
    let realmBreed = RealmBreed()
    realmBreed.name = transient.name
    return realmBreed
  }
  
  func toTransient() -> Breed {
    return Breed(name: name)
  }
}
