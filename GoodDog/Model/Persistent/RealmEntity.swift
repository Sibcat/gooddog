//
//  RealmEntity.swift
//  GoodDog
//
//  Created by Lena on 17.11.17.
//  Copyright © 2017 skvortsovaeo. All rights reserved.
//

import Foundation
import RealmSwift

protocol RealmEntity {
  associatedtype TransientType
  
  static func from(transient: TransientType) -> Self
  func toTransient() -> TransientType
}
