//
//  RealmMappedCollection.swift
//  GoodDog
//
//  Created by Lena on 17.11.17.
//  Copyright © 2017 skvortsovaeo. All rights reserved.
//

import Foundation
import RealmSwift

class RealmMappedCollection<Element: Object, Transformed> {
  typealias Transform = (Element) -> Transformed
  typealias T = Transformed
  
  private let realm: Realm
  
  private var sourceElements: AnyRealmCollection<Element>?
  private var results: AnyRealmCollection<Element>
  private let transform: Transform
  private var notificationToken: NotificationToken?
  
  var predicate: NSPredicate? = nil {
    didSet {
      refetchResults()
    }
  }
  
  var sortDescriptors: [SortDescriptor] {
    didSet {
      refetchResults()
    }
  }
  
  var notificationBlock: ((RealmCollectionChange<AnyRealmCollection<Element>>) -> Void)? {
    didSet {
      subscribeToResultsNotifications()
    }
  }
  
  init(realm: Realm,
       sourceElements: AnyRealmCollection<Element>,
       sortDescriptors: [SortDescriptor] = [],
       transform: @escaping Transform) {
    self.sourceElements = sourceElements
    self.sortDescriptors = sortDescriptors
    self.realm = realm
    self.transform = transform
    self.results = AnyRealmCollection(sourceElements.sorted(by: sortDescriptors))
  }
  
  init(realm: Realm, predicate: NSPredicate? = nil, sortDescriptors: [SortDescriptor], transform: @escaping Transform) {
    self.realm = realm
    self.transform = transform
    self.sortDescriptors = sortDescriptors
    self.predicate = predicate
    if let predicate = predicate {
      self.results = AnyRealmCollection(realm.objects(Element.self).filter(predicate)
        .sorted(by: sortDescriptors))
    } else {
      self.results = AnyRealmCollection(realm.objects(Element.self).sorted(by: sortDescriptors))
    }
  }
  
  deinit {
    unsubscribeFromResultsNotifications()
  }
  
  var subscribedToResultsNotifications: Bool {
    return notificationToken != nil
  }
  
  func subscribeToResultsNotifications() {
    unsubscribeFromResultsNotifications()
    notificationToken = results.observe { [unowned self] changes in
      self.notificationBlock?(changes)
    }
  }
  
  func unsubscribeFromResultsNotifications() {
    notificationToken?.invalidate()
  }
  
  private func refetchResults() {
    if let sourceElements = sourceElements {
      results = AnyRealmCollection(sourceElements.sorted(by: sortDescriptors))
    } else {
      results = AnyRealmCollection(realm.objects(Element.self).sorted(by: sortDescriptors))
    }
    if let predicate = predicate {
      results = AnyRealmCollection(results.filter(predicate))
    }
    subscribeToResultsNotifications()
  }
  
  var count: Int {
    return results.count
  }
  
  var isEmpty: Bool {
    return results.isEmpty
  }
  
  func item(at index: Int) -> Transformed? {
    guard index >= 0 && index < results.count else {
      return nil
    }
    
    return transform(results[index])
  }
  
  func index(of item: Element) -> Int? {
    return results.index(of: item)
  }
  
  func takeLast(limit: Int) -> [Transformed] {
    return results.suffix(limit).flatMap(self.transform)
  }
  
  func startReceivingUpdateNotifications() {
    if !subscribedToResultsNotifications {
      subscribeToResultsNotifications()
    }
  }
  
  func stopReceivingUpdateNotifications() {
    unsubscribeFromResultsNotifications()
  }
}
