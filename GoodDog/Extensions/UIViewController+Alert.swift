//
//  UIViewController+Alert.swift
//  GoodDog
//
//  Created by Lena on 17.11.17.
//  Copyright © 2017 skvortsovaeo. All rights reserved.
//

import UIKit

// MARK: - Alerts

extension UIViewController {

  func showAlert(forError error: Error) {
    showAlert(withMessage: error.localizedDescription)
  }
  
  func showAlert(withMessage message: String,
                 showCancelButton: Bool = false,
                 okButtonTitle: String = "Ok",
                 isOkButtonDestructive: Bool = false,
                 cancelButtonTitle: String = "Cancel",
                 tapBlock: (() -> Void)? = nil,
                 cancelTapBlock: (() -> Void)? = nil) {
    
    let cancelButtonTitle: String? = showCancelButton ? cancelButtonTitle : okButtonTitle
    let otherButtonTitle: String? = showCancelButton ? okButtonTitle : nil
    
    let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
    
    if let otherButtonTitle = otherButtonTitle {
      alertController.addAction(UIAlertAction(title: cancelButtonTitle, style: .cancel, handler: { _ in
        cancelTapBlock?()
      }))
      let okButtonStyle: UIAlertActionStyle = isOkButtonDestructive ? .destructive : .default
      alertController.addAction(UIAlertAction(title: otherButtonTitle, style: okButtonStyle, handler: { _ in
        tapBlock?()
      }))
    } else {
      alertController.addAction(UIAlertAction(title: cancelButtonTitle, style: .cancel, handler: { _ in
        tapBlock?()
      }))
    }
    showAlert(alertController)
  }
  
  func showAlert(_ alertController: UIAlertController) {
    present(alertController, animated: true, completion: nil)
  }
}
