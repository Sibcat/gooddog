//
//  Promise+Void.swift
//  GoodDog
//
//  Created by Lena on 17.11.17.
//  Copyright © 2017 skvortsovaeo. All rights reserved.
//

import Foundation
import PromiseKit

extension Promise {
  static var void: Promise<Void> {
    return Promise<Void>(value: ())
  }
}
