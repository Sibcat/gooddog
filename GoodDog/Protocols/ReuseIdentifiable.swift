//
//  ReuseIdentifiable.swift
//  GoodDog
//
//  Created by Lena on 15.11.17.
//  Copyright © 2017 skvortsovaeo. All rights reserved.
//

import UIKit

protocol ReuseIdentifiable {
  static var reuseIdentifier: String { get }
}

extension ReuseIdentifiable {
  static var reuseIdentifier: String {
    return String(describing: self)
  }
}

extension UITableViewCell: ReuseIdentifiable { }
extension UITableViewHeaderFooterView: ReuseIdentifiable { }
extension UICollectionReusableView: ReuseIdentifiable { }
