//
//  Coordinating.swift
//  GoodDog
//
//  Created by Lena on 14.11.17.
//  Copyright © 2017 skvortsovaeo. All rights reserved.
//

import Foundation

protocol Coordinating {
  func start()
}
