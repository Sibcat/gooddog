//
//  RealmFactory.swift
//  GoodDog
//
//  Created by Lena on 17.11.17.
//  Copyright © 2017 skvortsovaeo. All rights reserved.
//

import Foundation
import RealmSwift

class RealmFactory {
  static func realm() -> Realm {
    do {
      let realm = try Realm()
      realm.refresh()
      return realm
    } catch {
      fatalError("Can't create a Realm instance")
    }
  }

  static func inMemoryRealm() -> Realm {
    do {
      let config = Realm.Configuration(inMemoryIdentifier: "InMemory")
      let realm = try Realm(configuration: config)
      return realm
    } catch {
      fatalError("Can't create an in-memory Realm instance")
    }
  }
}
