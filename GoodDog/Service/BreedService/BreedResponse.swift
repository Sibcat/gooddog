//
//  BreedListResponse.swift
//  GoodDog
//
//  Created by Lena on 16.11.17.
//  Copyright © 2017 skvortsovaeo. All rights reserved.
//

import Foundation

struct BreedListResponse: NetworkResponse {
  var status: String
  var message: [String]
}

struct BreedImageResponse: NetworkResponse {
  var status: String
  var message: String
}
