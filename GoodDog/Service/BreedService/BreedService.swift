//
//  BreedService.swift
//  GoodDog
//
//  Created by Lena on 16.11.17.
//  Copyright © 2017 skvortsovaeo. All rights reserved.
//

import Foundation
import PromiseKit

class BreedService {
  
  private let networkService: NetworkService
  private let realmService: RealmService
  
  init(networkService: NetworkService, realmService: RealmService) {
    self.networkService = networkService
    self.realmService = realmService
  }
  
  func updateBreeds() -> Promise<Void> {
    return networkService.performRequest(method: .get,
                                         url: "breeds/list",
                                         parameters: nil).then { (breedListResponse: BreedListResponse) in
                                          let breeds: [Breed] = breedListResponse.message.map { Breed(name: $0) }
                                          return self.realmService.save(breeds)
    }
  }
  
  func fetchBreeds() -> RealmMappedCollection<RealmBreed, Breed> {
    let breeds: RealmMappedCollection<RealmBreed, Breed> = RealmMappedCollection(realm: realmService.getRealm(),
                                                                                 sortDescriptors: [],
                                                                                 transform: { breed -> Breed in
                                                                                  return breed.toTransient()
    })
    return breeds
  }
  
  func fetchImageURL(for breed: Breed) -> Promise<ImageURLHolder> {
    return networkService.performRequest(method: .get,
                                         url: "breed/\(breed.name)/images/random",
                                         parameters: nil).then { (breedListResponse: BreedImageResponse) in
                                          let holder = ImageURLHolder(urlString: breedListResponse.message)
                                          return Promise(value: holder)
    }
  }
}
