//
//  NetworkResponse.swift
//  GoodDog
//
//  Created by Lena on 16.11.17.
//  Copyright © 2017 skvortsovaeo. All rights reserved.
//

import Foundation

protocol NetworkResponse: Decodable {
  associatedtype T: Decodable
  var status: String { get }
  var message: T { get }
}

struct ErrorResponse: NetworkResponse {
  let code: String
  let status: String
  let message: String
}
