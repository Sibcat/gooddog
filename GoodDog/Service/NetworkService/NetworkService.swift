//
//  NetworkService.swift
//  GoodDog
//
//  Created by Lena on 16.11.17.
//  Copyright © 2017 skvortsovaeo. All rights reserved.
//

import Foundation
import Alamofire
import PromiseKit

class NetworkService {

  private struct Constants {
    static var baseURL: URL {
      guard let baseURL = URL(string: "https://dog.ceo/api/") else {
        fatalError("Can't construct dog.ceo base URL")
      }
      return baseURL
    }
  }
  
  func performRequest<T: Decodable>(method: HTTPMethod,
                                    url: String,
                                    parameters: Parameters? = nil,
                                    encoding: ParameterEncoding = URLEncoding.default,
                                    headers: HTTPHeaders? = nil) -> Promise<T> {
    return Promise { fulfill, reject in
      let request = Alamofire.request(Constants.baseURL.appendingPathComponent(url),
                                      method: method,
                                      parameters: parameters,
                                      encoding: encoding,
                                      headers: headers)
      request.responseJSON { response in
        self.handleBaseResponseJSON(response: response, fulfill: fulfill, reject: reject)
      }
    }
  }
  
  // MARK: - Helper functions
  
  private func handleBaseResponseJSON<T: Decodable>(response: DataResponse<Any>,
                                                    fulfill: (T) -> Void, reject: (Error) -> Void) {
    switch response.result {
    case .success:
      guard let data = response.data else {
        reject(NetworkError.unknown)
        break
      }
      let jsonDecoder = JSONDecoder()
      if let error = try? jsonDecoder.decode(ErrorResponse.self, from: data) {
        reject(NetworkError.networkError(statusCode: error.code))
        break
      }
      do {
        let object = try jsonDecoder.decode(T.self, from: data)
        fulfill(object)
      } catch {
        reject(error)
      }
      
    case .failure(let error):
      reject(error)
    }
  }
}
