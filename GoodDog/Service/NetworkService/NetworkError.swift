//
//  NetworkError.swift
//  GoodDog
//
//  Created by Lena on 17.11.17.
//  Copyright © 2017 skvortsovaeo. All rights reserved.
//

import Foundation

enum NetworkError: Error {
  case unknown
  case networkError(statusCode: String)
}

extension NetworkError: LocalizedError {
  public var errorDescription: String? {
    switch self {
    case .unknown:
      return NSLocalizedString("A user-friendly description of the error.", comment: "My error")
    case .networkError(let statusCode):
      return NSLocalizedString("network.error.\(statusCode)", tableName: "NetworkError", bundle: Bundle.main,
                               value: "", comment: "")
    }
  }
}
