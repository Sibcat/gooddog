//
//  AppDependencyContainer.swift
//  GoodDog
//
//  Created by Lena on 16.11.17.
//  Copyright © 2017 skvortsovaeo. All rights reserved.
//

import Foundation

protocol HasBreedService {
  var breedService: BreedService { get }
}

struct AppDependencyContainer: HasBreedService {
  let breedService: BreedService
  
  static var `default`: AppDependencyContainer {
    
    let networkService = NetworkService()
    let realmService = RealmService.persistent()
    
    let breedService = BreedService(networkService: networkService, realmService: realmService)
    
    return AppDependencyContainer(breedService: breedService)
  }
}
