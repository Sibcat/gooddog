//
//  AppCoordinator.swift
//  GoodDog
//
//  Created by Lena on 14.11.17.
//  Copyright © 2017 skvortsovaeo. All rights reserved.
//

import UIKit

class AppCoordinator: Coordinating {
  
  private let window: UIWindow
  private var breedListCoordinator: BreedListCoordinator?
  private let dependencyContainer: AppDependencyContainer
  
  init(window: UIWindow, dependencyContainer: AppDependencyContainer = .default) {
    self.window = window
    self.dependencyContainer = dependencyContainer
  }
  
  func start() {
    let navigationController = UINavigationController()
    window.rootViewController = navigationController
    
    breedListCoordinator = BreedListCoordinator(navigationController: navigationController,
                                                dependencyContainer: dependencyContainer)
    breedListCoordinator?.start()
    
    window.makeKeyAndVisible()
  }
}
