//
//  AppDelegate.swift
//  GoodDog
//
//  Created by Lena on 14.11.17.
//  Copyright © 2017 skvortsovaeo. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?
  private lazy var appCoordinator: AppCoordinator = self.getAppCoordinator()
  
  private func getAppCoordinator() -> AppCoordinator {
    let windowFrame = UIScreen.main.bounds
    let window = UIWindow(frame: windowFrame)
    window.backgroundColor = .white
    self.window = window
    
    let coordinator = AppCoordinator(window: window)
    return coordinator
  }

  func application(_ application: UIApplication,
                   didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    appCoordinator.start()
    return true
  }

}
